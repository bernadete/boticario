Este projeto tenta atender as melhores práticas descritas no site oficial do
Protractor.

A arquitetura do projeto está descrita abaixo:
1. O projeto encontra-se na pasta: "tests/e2e";

2. O arquivo "protractor.conf.js" contém todas as configurações necessárias para
execução dos testes, como endereço do selenium, os browsers onde os testes serão
executados, e também a baseUrl do projeto.
Neste projeto a configuração é para que os testes rodem no browser local da máquina do usuário.

3. Na pasta "page-objects" consta a especificação dos elementos por página.
Não é o teste em si. Esta é a forma de melhor manutenção e separação de
responsabilidades.

4. Na pasta "specs" contém os testes, especificados por páginas. Cada arquivo "spec"
tem seção de "describe", que descreve a suite a ser testada. E possui também os
casos de testes descritos nas seções "it".

5. Na pasta "data" contém a massa de dados utilizada nos testes:
Arquivos:
usuario.js - Informações do usuário a serem cadastradas

6. Na pasta "results" contem um relatório gerado a cada execução dos testes.
Neste relatório é possivel visualizar os screenshots de cada cenário executado, bem como mais informações.
Nome do relatório: results.html

7. Para esperas explícitas é utilizada a lib protractorHelper. O link da documentação está ao final da página.

Para rodar os testes localmente via terminal:
* npm run test

Considerações importantes:
Neste projeto estamos usando Protractor com Mocha e Chai (plugins: chai e chai as promissed)

- Se for necessário rodar somente uma suíte de testes ou somente um teste específico,
alterar o "describe" ou "it", acrescentando a palavra "only" na suíte ou teste.
Exemplo: describe.only / it.only

- Se for necessário pular uma suíte de testes ou somente um teste específico,
alterar o "describe" ou "it", acrescentando a palavra "skip" na suíte ou teste.
Exemplo: describe.skip / it.skip


Para maiores informações, veja a documentação oficial das ferramentas:
* Protractor - "http://www.protractortest.org/#/"
* Mocha - "https://mochajs.org/"
* Chai - "http://chaijs.com/"
* Protractor Helper - https://github.com/wlsf82/protractor-helper
