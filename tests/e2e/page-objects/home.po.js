
class Home {
  constructor() {
    this.selecionaProduto = element.all(by.css('.product_img_link[title="Blouse"]')).first();
    this.adicionaProdutoCarrinho = element(by.css('.box-cart-bottom #add_to_cart .exclusive'));
    this.botaoIrCheckout = element(by.css('.btn-default[title="Proceed to checkout"]'));
  }

  static visit() {
    browser.get('/');
  }
}

module.exports = Home;
