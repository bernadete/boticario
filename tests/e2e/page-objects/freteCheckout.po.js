class Frete {
  constructor() {
    this.aceiteTermos = element(by.css('.checker'));
    this.confirmaFrete = element(by.css('#form button[type="submit"]'));
  }
}

module.exports = Frete;
