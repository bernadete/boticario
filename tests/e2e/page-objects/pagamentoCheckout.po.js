class Pagamento {
  constructor() {
    this.confirmaPagamento = element(by.css('.bankwire'));
    this.confirmaCompra = element(by.css('#cart_navigation .button'));
    this.valorTotalCompra = element(by.css('#total_price'));
    this.finalCompra = element(by.css('.page-heading'));
  }
}

module.exports = Pagamento;
