class Conta {
  constructor() {
    this.email = element(by.id('email_create'));
    this.botaoCriarConta = element(by.id('SubmitCreate'));
    this.primeiroNome = element(by.id('customer_firstname'));
    this.ultimoNome = element(by.id('customer_lastname'));
    this.senha = element(by.id('passwd'));
    this.endereco = element(by.id('address1'));
    this.cep = element(by.id('postcode'));
    this.pais = element(by.id('id_country'));
    this.celular = element(by.id('phone_mobile'));
    this.cidade = element(by.id('city'));
    this.estado = element(by.id('id_state'));
    this.botaoRegistrar = element(by.id('submitAccount'));
  }

  criarUsuario(user) {
    this.primeiroNome.sendKeys(user.primeiroNome);
    this.ultimoNome.sendKeys(user.ultimoNome);
    this.senha.sendKeys(user.senha);
    this.celular.sendKeys(user.celular);
    this.endereco.sendKeys(user.endereco);
    this.cep.sendKeys(user.cep);
    this.cidade.sendKeys(user.cidade);
    this.estado.$(user.estado).click();
    this.pais.$(user.pais).click();
    this.botaoRegistrar.click();
  }
}

module.exports = Conta;
