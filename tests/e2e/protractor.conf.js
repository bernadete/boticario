module.exports.config = {
  framework:'mocha',
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect:	true,
  capabilities: {
    'browserName':'chrome'
  },
  specs: ['specs/*_specs.js'],
  baseUrl: 'http://www.automationpractice.com',


  onPrepare(){
    browser.driver.manage().window().maximize();
    browser.manage().timeouts().pageLoadTimeout(5000);
    browser.manage().timeouts().implicitlyWait(4000);
    browser.ignoreSynchronization = true;
  },

  //Resultado dos Testes *COM* Screenshots
  mochaOpts: {
   showColors: true,
    verbose: true,
    timeout: 40000,
    reporter: 'mochawesome-screenshots',
    reporterOptions: {
      reportDir: './tests/results',
      reportName: 'results',
      reportTitle: 'Results automationpractice',
      reportPageTitle: 'Results',
      takePassedScreenshot: true, // Option to control the scope of screenshots
      clearOldScreenshots: true,
      jsonReport: false,
      multiReport: false // Use 'multiReport = true' for parallel test execution
    }
  }
}
