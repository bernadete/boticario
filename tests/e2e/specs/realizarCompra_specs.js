const chai = require('chai');
const protractorHelper = require('protractor-helper');
const Home = require('../page-objects/home.po.js');
const Checkout = require('../page-objects/resumoCheckout.po.js');
const Conta = require('../page-objects/contaCheckout.po.js');
const Endereco = require('../page-objects/enderecoCheckout.po.js');
const Frete = require('../page-objects/freteCheckout.po.js');
const Pagamento = require('../page-objects/pagamentoCheckout.po.js')
const dadosUsuario = require('../../data/usuario.js');
const chaiAsPromised = require('chai-as-promised');

const expect = chai.expect;
chai.use(chaiAsPromised);

const home = new Home();
const checkout = new Checkout();
const conta = new Conta();
const endereco = new Endereco();
const frete = new Frete();
const pagamento = new Pagamento();


before('Entrar no site', () => {
  Home.visit();
});

after('Deletar cookies', () => {
  browser.driver.manage().deleteAllCookies();
  browser.restart();

});

describe('Dado que estou na home', () => {
  describe('Quando seleciono um produto', () => {
    it('Então finalizo a compra com sucesso', () => {
      home.selecionaProduto.click();

      browser.switchTo().frame(element(by.css('.fancybox-iframe')).getWebElement());

      home.adicionaProdutoCarrinho.click();
      protractorHelper.clickWhenClickable(home.botaoIrCheckout, 2000);

      //verifica se o produto existe na página
      const codigoproduto = checkout.codigoProdutoSelecionado;
      expect(codigoproduto).to.equal(codigoproduto);


      checkout.botaoConfirmaCheckout.click();

      // criando conta de usuario
      conta.email.sendKeys(dadosUsuario.emailUsuario.email);
      conta.botaoCriarConta.click();
      conta.criarUsuario(dadosUsuario.usuario);

      //verifica se o endereço preenchido é o endereço exibido
      const enderecoPreenchido = endereco.endereco;
      expect(enderecoPreenchido.getText()).to.eventually.equal(dadosUsuario.usuario.endereco);

      endereco.confirmaEndereco.click();

      frete.aceiteTermos.click();
      frete.confirmaFrete.click();

      //verifica se o valor da compra está correto
      const valorTotal = pagamento.valorTotalCompra;
      expect(valorTotal.getText()).to.eventually.equal('$29.00');

      pagamento.confirmaPagamento.click();
      pagamento.confirmaCompra.click();

      // verifica se compra finalizou com Sucesso
      const mensagemSucesso = pagamento.finalCompra;
      expect(mensagemSucesso.getText()).to.eventually.equal('ORDER CONFIRMATION');
    });
  });
});
