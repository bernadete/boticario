const faker = require('faker');


emailUsuario = {
  email: faker.internet.email()
};

usuario = {
  primeiroNome: faker.name.firstName(),
  ultimoNome: faker.name.lastName(),
  senha: '12345678',
  endereco: faker.address.streetAddress(),
  cidade: faker.address.city(),
  cep: '58745',
  estado:'option[value="5"]',
  pais: 'option[value="21"]',
  celular: '9852362541'
};

module.exports = {
  emailUsuario,
  usuario
};
